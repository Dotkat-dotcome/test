# temp

Small experiment to test machine A40 and A100 on Lab-ia

## for environment
`conda env create -f environment.yml` or `pip install -r requirements.txt`

## run the experiment
`sbatch run.slurm`
You can specify the number of GPU in the slurm file, currently set to 2
